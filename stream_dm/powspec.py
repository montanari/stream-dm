# Adapted from Jo Bovy's
# streamgap-pepper/StreamPepperAnalysisGD1LikeObserved.ipynb, see also
# streamgap_pepper/LICENSE.

# Copyright 2020 Francesco Montanari

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Stream power spectra comparison in different mass ranges."""

import os

import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

import pdf as pdf
import powspec_utils as psu
import stream_utils as stu


def plot_powspec(datadir, savedir):
    ll, X_dens, y_dens = stu.load_data_Qrho(os.path.join(
        datadir, 'scripts_data/Xy_obs_density_2classes.pkl'),
                                            sigma_shot=0.)
    X_dens0, X_dens1 = X_dens[y_dens == 0], X_dens[y_dens == 1]

    fig, ax = plt.subplots()

    def _plt(X, err=None, ls='-', label=None):
        # Fill interquantile range.
        plotx, __, low, high, __ = psu.median_csd_obs(ll,
                                                      X,
                                                      scatter=True,
                                                      coord1='dens',
                                                      coord2='dens')
        plt.fill_between(plotx, low, high, color='0.7', alpha=0.5)
        # Plot median.
        px, py, py_err = psu.median_csd_obs(ll,
                                            X,
                                            coord1='dens',
                                            coord2='dens',
                                            err=err)
        ax.loglog(px, py, lw=2., ls=ls, label=label)
        if err is not None:
            plt.loglog(px, py_err, color='k')

    _plt(X_dens0, err=0.1, ls='--', label='$10^3-10^5 M_\\odot$')
    _plt(X_dens1, ls=':', label='$10^5-10^9 M_\\odot$')
    ax.set_xlim(1, 100)
    ax.set_ylim(0.01, 10.)
    ax.set_xlabel(r'$1/k_l\,(\mathrm{deg})$')
    ax.set_ylabel(r'$P_{Q\rho Q\rho}$')
    ax.legend(loc='upper left')
    fig.tight_layout()
    pdf._savefig(fig, os.path.join(savedir, 'powspec.pdf'))


if __name__ == '__main__':
    pdf.set_matplotlib_font(14)
    DATADIR = '/home/francesco/Data/streams/'
    SAVEDIR = '/home/francesco/Dropbox/Work/streams/plots/'
    plot_powspec(DATADIR, SAVEDIR)

# Adapted from Jo Bovy's
# streamgap-pepper/StreamPepperAnalysisGD1Like.ipynb, see also
# streamgap_pepper/LICENSE.

# Copyright 2020 Francesco Montanari

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Utilities to analyze the stream power spectrum.

.. note:: We rename Bovy's `streamgap_pepper` variable $\delta$ to $Q\rho$.
"""

import glob

from galpy.util import bovy_plot
import matplotlib
matplotlib.use('PDF')
from matplotlib import cm, pyplot
from matplotlib.pyplot import *
from matplotlib.ticker import FuncFormatter, NullFormatter
import numpy
from scipy import signal
import seaborn as sns


def median_csd(filename, filename2=None, scatter=False, scaling='spectrum'):
    """Compute the median cross power spectral density (CSD),
       if filename2 is not None, compute cross.
    """
    data1 = numpy.genfromtxt(filename, delimiter=',', max_rows=5002)
    # Search for batches
    batchfilenames = glob.glob(filename.replace('.dat', '.*.dat'))
    for bfilename in batchfilenames:
        try:
            datab = numpy.genfromtxt(bfilename, delimiter=',')
        except:
            continue
        data1 = numpy.vstack((data1, datab[2:]))
    if filename2 is None:
        data2 = data1
    else:
        data2 = numpy.genfromtxt(filename2, delimiter=',', max_rows=5002)
        # Search for batches
        batchfilenames = glob.glob(filename2.replace('.dat', '.*.dat'))
        for bfilename in batchfilenames:
            try:
                datab = numpy.genfromtxt(bfilename, delimiter=',')
            except:
                continue
            data2 = numpy.vstack((data2, datab[2:]))
    apars = data1[0]
    nsim = len(data1) - 2  # first two are apar and smooth
    if nsim < 1000:
        print((
            "WARNING: Using fewer than 1,000 (%i) simulations to compute medians from %s"
            % (nsim, filename)))
    px = signal.csd(data1[1],
                    data2[1],
                    fs=1. / (apars[1] - apars[0]),
                    scaling=scaling,
                    nperseg=len(apars))[0]
    ppy = numpy.empty((nsim, len(px)))
    for ii in range(nsim):
        if numpy.any(data1 != data2):
            ppy[ii] = numpy.absolute(
                signal.csd(data1[2 + ii] / data1[1],
                           data2[2 + ii] / data2[1],
                           fs=1. / (apars[1] - apars[0]),
                           scaling=scaling,
                           nperseg=len(apars))[1])
        else:
            ppy[ii] = signal.csd(data1[2 + ii] / data1[1],
                                 data1[2 + ii] / data1[1],
                                 fs=1. / (apars[1] - apars[0]),
                                 scaling=scaling,
                                 nperseg=len(apars))[1].real
    # Following is the correct scaling in the sense that random noise
    # gives same CSD no matter how long the stream.
    if not scatter:
        return (180. / px / numpy.pi,
                numpy.sqrt(
                    numpy.nanmedian(ppy, axis=0) * (apars[-1] - apars[0])))
    else:
        out = numpy.sqrt(numpy.nanmedian(ppy, axis=0) * (apars[-1] - apars[0]))
        ppy.sort(axis=0)
        return (180. / px / numpy.pi, out,
                numpy.sqrt(ppy[int(numpy.round(0.25 * nsim))] *
                           (apars[-1] - apars[0])),
                numpy.sqrt(ppy[int(numpy.round(0.75 * nsim))] *
                           (apars[-1] - apars[0])))


def median_csd_obs(ll, X_dens, scatter=False, coord1=1, coord2=1, err=None):
    """Compute the median cross power spectral density (CSD) based on the
    observable longitude ll and density contrast Qrho, encoded in the X_dens
    tensor of shape (samples, longitude_bins).
    """
    nsim = len(X_dens)
    if nsim < 1000:
        print(
            f"WARNING: Using fewer than 1,000 ({nsim}) simulations to compute medians"
        )
    scaling = 'spectrum'
    px = signal.csd(X_dens[0],
                    X_dens[0],
                    fs=1. / (ll[1] - ll[0]),
                    scaling=scaling,
                    nperseg=len(ll))[0]
    ppy = np.empty((nsim, len(px)))
    ppy_err = np.empty((nsim, len(px)))
    for ii in range(nsim):
        Qrho = X_dens[ii]
        ppy[ii] = signal.csd(Qrho,
                             Qrho,
                             fs=1. / (ll[1] - ll[0]),
                             scaling=scaling,
                             nperseg=len(ll))[1].real
        # Same for errors
        if err is not None:
            tmock = err * np.random.normal(size=len(ll))
            ppy_err[ii] = signal.csd(tmock,
                                     tmock,
                                     fs=1. / (ll[1] - ll[0]),
                                     scaling=scaling,
                                     nperseg=len(ll))[1].real
    # Following is the correct scaling in the sense that random noise gives
    # same CSD no matter how long the stream.
    if not scatter:
        return (1. / px, np.sqrt(np.nanmedian(ppy, axis=0) * (ll[-1] - ll[0])),
                np.sqrt(np.nanmedian(ppy_err, axis=0) * (ll[-1] - ll[0])))
    else:
        out = np.sqrt(np.nanmedian(ppy, axis=0) * (ll[-1] - ll[0]))
        ppy.sort(axis=0)
        return (1. / px, out,
                np.sqrt(ppy[int(np.round(0.25 * nsim))] * (ll[-1] - ll[0])),
                np.sqrt(ppy[int(np.round(0.75 * nsim))] * (ll[-1] - ll[0])),
                np.sqrt(np.nanmedian(ppy_err, axis=0) * (ll[-1] - ll[0])))


def plot_all(filename,
             color=sns.color_palette()[0],
             zorder=1,
             ls='-',
             lw=2.,
             fill=False,
             fill_color='0.65',
             fill_zorder=0,
             scale=1.):
    subplot(1, 3, 1)
    px, py = median_csd(filename)
    loglog(px, scale * py, lw=lw, color=color, zorder=zorder, ls=ls)
    if fill:
        plotx, dum, low, high = median_csd(filename, scatter=True)
        fill_between(plotx,
                     scale * low,
                     scale * high,
                     color=fill_color,
                     zorder=fill_zorder,
                     alpha=0.5)
    subplot(1, 3, 2)
    px, py = median_csd(filename.replace('dens', 'omega'))
    loglog(px, scale * py, lw=lw, color=color, zorder=zorder, ls=ls)
    if fill:
        plotx, dum, low, high = median_csd(filename.replace('dens', 'omega'),
                                           scatter=True)
        fill_between(plotx,
                     scale * low,
                     scale * high,
                     color=fill_color,
                     zorder=fill_zorder,
                     alpha=0.5)
    subplot(1, 3, 3)
    px, py = median_csd(filename, filename.replace('dens', 'omega'))
    loglog(px, scale * py, lw=lw, color=color, zorder=zorder, ls=ls)
    if fill:
        plotx, dum, low, high = median_csd(filename,
                                           filename.replace('dens', 'omega'),
                                           scatter=True)
        fill_between(plotx,
                     scale * low,
                     scale * high,
                     color=fill_color,
                     zorder=fill_zorder,
                     alpha=0.5)
    return None


def set_ranges_and_labels():
    subplot(1, 3, 1)
    bovy_plot.bovy_text(r'$\sqrt{Q\rho Q\rho}$', top_left=True, size=18.)
    xlabel(r'$1/k_{\theta}\,(\mathrm{deg})$')
    ylim(0.0001, 1.)
    xlim(1., 100.)
    for axis in [gca().xaxis, gca().yaxis]:
        axis.set_major_formatter(
            FuncFormatter(lambda y, pos: (r'${{:.{:1d}f}}$'.format(
                int(numpy.maximum(-numpy.log10(y), 0)))).format(y)))
    subplot(1, 3, 2)
    bovy_plot.bovy_text(r'$\sqrt{\Omega\Omega}$', top_left=True, size=18.)
    xlabel(r'$1/k_{\theta}\,(\mathrm{deg})$')
    ylim(0.0001, 1.)
    xlim(1., 100.)
    gca().xaxis.set_major_formatter(
        FuncFormatter(lambda y, pos: (r'${{:.{:1d}f}}$'.format(
            int(numpy.maximum(-numpy.log10(y), 0)))).format(y)))
    gca().yaxis.set_major_formatter(NullFormatter())
    subplot(1, 3, 3)
    bovy_plot.bovy_text(r'$\sqrt{|Q\rho \Omega|}$', top_left=True, size=18.)
    xlabel(r'$1/k_{\theta}\,(\mathrm{deg})$')
    ylim(0.0001, 1.)
    xlim(1., 100.)
    gca().xaxis.set_major_formatter(
        FuncFormatter(lambda y, pos: (r'${{:.{:1d}f}}$'.format(
            int(numpy.maximum(-numpy.log10(y), 0)))).format(y)))
    gca().yaxis.set_major_formatter(NullFormatter())
    return None


def color_from_colormap(val, cmap, cmin, cmax):
    return cmap((val - cmin) / (cmax - cmin))


def add_colorbar(vmin, vmax, clabel, save_figures=False, fmt=r'$%.1f$'):
    fig = pyplot.gcf()
    if save_figures:
        cbar_ax = fig.add_axes([0.9, 0.135, 0.025, 0.815])
    else:
        fig.subplots_adjust(right=0.95)
        cbar_ax = fig.add_axes([0.975, 0.13, 0.025, 0.83])
    sm = pyplot.cm.ScalarMappable(cmap=cmap,
                                  norm=pyplot.Normalize(vmin=vmin, vmax=vmax))
    sm._A = []
    cbar = fig.colorbar(sm, cax=cbar_ax, use_gridspec=True, format=fmt)
    cbar.set_label(clabel)
    return None


def add_discrete_colorbar(vmin,
                          vmax,
                          clabel,
                          ticks,
                          cmap,
                          save_figures=False,
                          fmt=r'$%.1f$'):
    fig = pyplot.gcf()
    if save_figures:
        cbar_ax = fig.add_axes([0.9, 0.135, 0.025, 0.815])
    else:
        fig.subplots_adjust(right=0.95)
        cbar_ax = fig.add_axes([0.975, 0.13, 0.025, 0.83])
    tcmap = matplotlib.colors.ListedColormap(
        [cmap(f) for f in numpy.linspace(0., 1., len(ticks))])
    dtick = (ticks[1] - ticks[0])
    sm = pyplot.cm.ScalarMappable(cmap=tcmap,
                                  norm=pyplot.Normalize(
                                      vmin=vmin - 0.5 * dtick,
                                      vmax=vmax + 0.5 * dtick))
    sm._A = []
    cbar = fig.colorbar(sm,
                        cax=cbar_ax,
                        use_gridspec=True,
                        format=fmt,
                        ticks=ticks)
    cbar.set_label(clabel)
    return None

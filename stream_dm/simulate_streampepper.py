"""Run streamgap_pepper simulations."""

from streamgap_pepper.simulate_streampepper import run

if __name__ == '__main__':
    run()

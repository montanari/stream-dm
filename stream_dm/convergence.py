# Adapted from Jo Bovy's
# streamgap-pepper/StreamPepperAnalysisGD1Like.ipynb, see also
# streamgap_pepper/LICENSE.

# Copyright 2020 Francesco Montanari

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Convergence tests for simulations."""

import os

from galpy.util import bovy_plot
import numpy
from matplotlib import cm, pyplot
from matplotlib.pyplot import *
import seaborn as sns

import powspec_utils as psu
import streamgap_pepper.gd1_util as gd1_util


def plot_single_times(mass='3-5',
                      y1_y2_ylabel_3Gyr=(0.02, 0.04, 0.045),
                      y1_y2_ylabel_1Gyr=(0.001, 0.0004, 0.0002),
                      basefilename='',
                      save_figures=True):
    bovy_plot.bovy_print(axes_labelsize=18.,
                         xtick_labelsize=14.,
                         ytick_labelsize=14.)
    if save_figures:
        rcParams['figure.figsize'] = [11, 4.25]
        fig = figure()
        fig.subplots_adjust(left=0.065,
                            right=0.8875,
                            bottom=0.135,
                            top=0.95,
                            wspace=0.075)
    else:
        rcParams['figure.figsize'] = [16, 6]
    cmap = cm.plasma

    psu.plot_all(os.path.join(basefilename,
                             'gd1_t0.81peri_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(0., cmap, 0., 1.),
                zorder=6)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t3.21peri_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(0., cmap, 0., 1.),
                zorder=6)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t1.01apo_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(1., cmap, 0., 1.),
                zorder=4)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t3.01apo_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(1., cmap, 0., 1.),
                zorder=4)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t0.9between_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(0.5, cmap, 0., 1.),
                zorder=3,
                fill=True)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t3.1between_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(0.5, cmap, 0., 1.),
                zorder=3,
                fill=True)

    psu.set_ranges_and_labels()
    subplot(1, 3, 1)
    plot([23., 27], [y1_y2_ylabel_3Gyr[0], y1_y2_ylabel_3Gyr[1]], 'k-', lw=1.)
    bovy_plot.bovy_text(8.,
                        y1_y2_ylabel_3Gyr[2],
                        r'$t_{\mathrm{impact}} \approx 3\,\mathrm{Gyr}$',
                        size=18.)
    plot([45., 55.], [y1_y2_ylabel_1Gyr[0], y1_y2_ylabel_1Gyr[1]], 'k-', lw=1.)
    bovy_plot.bovy_text(8.5,
                        y1_y2_ylabel_1Gyr[2],
                        r'$t_{\mathrm{impact}} \approx 1\,\mathrm{Gyr}$',
                        size=18.)
    psu.add_discrete_colorbar(0,
                             1,
                             r'$\mathrm{relative\ time\ wrt\ pericenter}$',
                             [0., 0.5, 1.],
                             cmap,
                             save_figures=save_figures)

    if save_figures:
        bovy_plot.bovy_end_print(
            os.path.join(basefilename,
                         'gd1like_periapobetween_' + mass + '.pdf'))


def plot_multiple_times(mass='3-5', basefilename='', save_figures=True):
    bovy_plot.bovy_print(axes_labelsize=18.,
                         xtick_labelsize=14.,
                         ytick_labelsize=14.)
    cmap = cm.plasma
    if save_figures:
        rcParams['figure.figsize'] = [11, 4.25]
        fig = figure()
        fig.subplots_adjust(left=0.065,
                            right=0.8875,
                            bottom=0.135,
                            top=0.95,
                            wspace=0.075)
    else:
        rcParams['figure.figsize'] = [16, 6]
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t1sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(0, cmap, 0., 8.),
                zorder=6)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t2sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(1, cmap, 0., 8.),
                zorder=6)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t4sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(2, cmap, 0., 8.),
                zorder=4)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t8sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(3, cmap, 0., 8.),
                zorder=4)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t16sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(4, cmap, 0., 8.),
                zorder=3)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t32sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(5, cmap, 0., 8.),
                zorder=3)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t64sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(6, cmap, 0., 8.),
                zorder=3,
                fill=True)
    psu.set_ranges_and_labels()
    psu.add_discrete_colorbar(0,
                             6,
                             r'$\log_2 N_{\mathrm{times}}$',
                             [0, 1, 2, 3, 4, 5, 6],
                             cmap,
                             fmt=r'$%i$',
                             save_figures=save_figures)
    if not save_figures: rcParams['figure.tight_layout'] = True
    if save_figures:
        bovy_plot.bovy_end_print(
            os.path.join(basefilename,
                         'gd1like_timesampling_' + mass + '.pdf'))


def plot_max_X(mass='3-5', basefilename='', save_figures=True):
    bovy_plot.bovy_print(axes_labelsize=18.,
                         xtick_labelsize=14.,
                         ytick_labelsize=14.)
    cmap = cm.plasma
    if save_figures:
        rcParams['figure.figsize'] = [11, 4.25]
        fig = figure()
        fig.subplots_adjust(left=0.065,
                            right=0.8875,
                            bottom=0.135,
                            top=0.95,
                            wspace=0.075)
    else:
        rcParams['figure.figsize'] = [16, 6]
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t32sampling_X1_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(1, cmap, 0., 10.),
                zorder=3)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t32sampling_X3_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(3, cmap, 0., 10.),
                zorder=3)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t32sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(5, cmap, 0., 10.),
                zorder=3)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t32sampling_X10_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(10., cmap, 0., 10.),
                zorder=3,
                fill=True)
    psu.set_ranges_and_labels()
    # Custom colorbar
    vmin, vmax = 0., 10.
    fig = pyplot.gcf()
    if save_figures:
        cbar_ax = fig.add_axes([0.9, 0.135, 0.025, 0.815])
    else:
        fig.subplots_adjust(right=0.95)
        cbar_ax = fig.add_axes([0.975, 0.13, 0.025, 0.83])
    tcmap = matplotlib.colors.ListedColormap(
        [cmap(1. / 10.),
         cmap(3. / 10.),
         cmap(5. / 10.),
         cmap(1.)])
    bounds = [0, 2., 4., 6., 10.]
    norm = matplotlib.colors.BoundaryNorm(bounds, tcmap.N)
    sm = pyplot.cm.ScalarMappable(cmap=tcmap, norm=norm)
    sm._A = []
    cbar = matplotlib.colorbar.ColorbarBase(cbar_ax,
                                            cmap=tcmap,
                                            norm=norm,
                                            spacing='proportional',
                                            boundaries=bounds,
                                            ticks=[1., 3., 5., 8.])
    cbar.set_label(r'$X$')
    cbar.set_ticklabels([r'$1.0$', r'$3.0$', r'$5.0$', r'$10.0$'])
    if save_figures:
        bovy_plot.bovy_end_print(
            os.path.join(basefilename, 'gd1like_bmax_' + mass + '.pdf'))


def plot_lenfac(mass='3-5', basefilename='', save_figures=True):
    bovy_plot.bovy_print(axes_labelsize=18.,
                         xtick_labelsize=14.,
                         ytick_labelsize=14.)
    cmap = cm.plasma
    if save_figures:
        rcParams['figure.figsize'] = [11, 4.25]
        fig = figure()
        fig.subplots_adjust(left=0.065,
                            right=0.8875,
                            bottom=0.135,
                            top=0.95,
                            wspace=0.075)
    else:
        rcParams['figure.figsize'] = [16, 6]
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t32sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(1, cmap, 0.75, 1.25),
                zorder=3,
                fill=True)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t32sampling_lf1.25_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(1.25, cmap, 0.75, 1.25),
                zorder=4)
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t32sampling_lf0.75_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(0.75, cmap, 0.75, 1.25),
                zorder=4)
    psu.set_ranges_and_labels()
    psu.add_discrete_colorbar(0.75,
                             1.25,
                             r'$\mathrm{length\ multiplier}$',
                             [0.75, 1., 1.25],
                             cmap,
                             fmt=r'$%.2f$',
                             save_figures=save_figures)
    if save_figures:
        bovy_plot.bovy_end_print(
            os.path.join(basefilename, 'gd1like_lenfac_' + mass + '.pdf'))


def plot_profile(mass='3-5', basefilename='', save_figures=True):
    bovy_plot.bovy_print(axes_labelsize=18.,
                         xtick_labelsize=14.,
                         ytick_labelsize=14.)
    rcParams['figure.figsize'] = [16, 6]
    cmap = cm.plasma
    if save_figures:
        rcParams['figure.figsize'] = [11, 4.25]
        fig = figure()
        fig.subplots_adjust(left=0.065,
                            right=0.8875,
                            bottom=0.135,
                            top=0.95,
                            wspace=0.075)
    else:
        rcParams['figure.figsize'] = [16, 6]
    psu.plot_all(os.path.join(basefilename,
                             'gd1_t32sampling_X5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(0., cmap, numpy.log10(0.4),
                                             numpy.log10(2.5)),
                zorder=12,
                fill=True,
                lw=2.5)
    psu.plot_all(os.path.join(
        basefilename, 'gd1_t32sampling_X12p5_rsfacp4_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(numpy.log10(0.4), cmap,
                                             numpy.log10(0.4),
                                             numpy.log10(2.5)),
                zorder=9)
    psu.plot_all(os.path.join(
        basefilename, 'gd1_t32sampling_X2_rsfac2p5_' + mass + '_dens.dat'),
                color=psu.color_from_colormap(numpy.log10(2.5), cmap,
                                             numpy.log10(0.4),
                                             numpy.log10(2.5)),
                zorder=10)
    psu.plot_all(os.path.join(
        basefilename, 'gd1_t32sampling_X3p24_plum_' + mass + '_dens.dat'),
                color=sns.color_palette()[0],
                zorder=11)
    psu.set_ranges_and_labels()
    if not save_figures: rcParams['figure.tight_layout'] = True
    psu.add_discrete_colorbar(
        numpy.log10(0.4),
        numpy.log10(2.5),
        r'$\log_{10} r_s/r_{s,\mathrm{fid}}$',
        [numpy.log10(0.4), 0., numpy.log10(2.5)],
        cmap,
        save_figures=save_figures)
    if save_figures:
        subplot(1, 3, 1)
        plot([7., 12.], [0.000225, 0.000225],
             color=sns.color_palette()[0],
             lw=2.)
        bovy_plot.bovy_text(15., 0.000175, r'$\mathrm{Plummer}$', size=18.)
        plot([7., 12.], [0.0005, 0.00050625],
             color=psu.color_from_colormap(numpy.log10(0.4), cmap,
                                          numpy.log10(0.4), numpy.log10(2.5)),
             lw=2.)
        plot([7., 12.], [0.00045, 0.00045],
             color=psu.color_from_colormap(numpy.log10(1.), cmap,
                                          numpy.log10(0.4), numpy.log10(2.5)),
             lw=2.)
        plot(
            [7., 12.],
            [0.0004, 0.0004],
            color=psu.color_from_colormap(numpy.log10(2.5), cmap,
                                         numpy.log10(0.4), numpy.log10(2.5)),
            lw=2.,
        )
        bovy_plot.bovy_text(15., 0.000375, r'$\mathrm{Hernquist}$', size=18.)
        bovy_plot.bovy_end_print(
            os.path.join(basefilename, 'gd1like_rsplummer.pdf'))


if __name__ == '__main__':
    BASEFILENAME = "/home/francesco/Data/streams/20200524_convergence"
    sdf_smooth = gd1_util.setup_gd1model()
    print(("Stream length in arc degree and physical kpc",
           sdf_smooth.length(ang=True), sdf_smooth.length(phys=True)))

    # Single times.
    #
    # Impacts near pericenter, near apocenter, and in between [radial period is
    # 400 Myr].
    plot_single_times(mass='3-5',
                      y1_y2_ylabel_3Gyr=(0.08, 0.2, 0.25),
                      y1_y2_ylabel_1Gyr=(0.0015, 0.004, 0.0008),
                      basefilename=BASEFILENAME)
    # plot_single_times('4.0')

    # Multiple times.
    #
    # How finely do we have to sample the impact-time grid to get statistically
    # similar results?
    plot_multiple_times(mass='3-5', basefilename=BASEFILENAME)
    # plot_multiple_times(mass='4.0')

    # Maximum impact parameter.
    #
    # How many times the scale radius do we need to consider for the impact
    # parameter? We fix the number of potential impact times to 64 (which seems
    # close to converged, see above).
    plot_max_X(mass='3-5', basefilename=BASEFILENAME)
    # plot_max_X(mass='4.0')

    # How far along the stream do we have to allow impacts?
    #
    # We consider impacts out to length_factor times the length of the stream.
    # What is a good value of length_factor? (To have the same impact rate as
    # for the fiducial case we need to also change timescdm jointly with
    # length_factor.)
    plot_lenfac(mass='3-5', basefilename=BASEFILENAME)
    # plot_lenfac(mass='4.0')

    # Hernquist vs. Plummer profile.
    #
    # For consistency with the sampling of the fiducial Hernquist profile, both
    # $X$ and ${\rm rsfac}=r_s/r_{s, fid}$ need to change such that $X*{\rm
    # rsfac} = X_{fid}$.
    #
    # The Plummer sphere follows $r_s(M) = 1.62 {\rm kpc}\ (M / 10^8
    # M_\odot)^{0.5}$, whose prefactor differs from the fiducial Hernquist
    # profile $r_s(M) = 1.05 {\rm kpc}\ (M / 10^8 M_\odot)^{0.5}$. Hence, we
    # set $X=(1.05/1.62) X_{fid}$
    plot_profile(mass='3-5', basefilename=BASEFILENAME)

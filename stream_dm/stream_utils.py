# Copyright 2020 Francesco Montanari

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Utilities to analyze the output of `streampepper` simulations.

Format of data computed with `streamgap_pepper/simulate_streampepper.py`:

- The first row is the list of parallel angles (`apars`) where the
  density/frequency have been computed.

- The second row is the density or frequency of the smooth stream.

- The following rows are the density or frequency of perturbed streams.

.. note:: Densities are normalized to 1 close to the progenitor. See
          `streamgap-pepper/meanOperpAndApproxImpacts.ipynb` for the physical
          normalization, and `galpy.df.streamdf._density_par` for more details.

"""

import multiprocessing as mp
import pickle
import os

from galpy.util import save_pickles
import numpy as np
from scipy import interpolate

import streamgap_pepper.gd1_util as gd1_util
import streamgap_pepper.simulate_streampepper as simulate_streampepper


def add_noise(X, sigma):
    """Add Gaussian noise density profiles. `sigma` is fractional error
    relative to smooth density profile.
    """
    return np.array([np.random.normal(x, sigma) for x in X])


def _load_data(dfiles):
    """Load data and check that they have been computed at the same apars."""
    data = np.array([np.loadtxt(df, delimiter=",") for df in dfiles])
    apars = data[0][0]
    if data.size > 1:
        if not np.all([
                np.all(np.isclose(apars, d))
                for d in list(map(lambda x: x[0], data[1:]))
        ]):
            raise NotImplementedError(
                "Data must be computed at the same parallel angles.")
    return data


def _get_shuffled_X_y(*Xs):
    """Concatenate X0 and X1, adding 0 and 1 labels to y array. Return
    shuffled (X, y) data.
    """
    X = np.concatenate(Xs)
    ys = [np.full(X.shape[0], i) for i, X in enumerate(Xs)]
    y = np.concatenate(ys)
    if X.shape[0] != y.shape[0]:
        raise RuntimeError(
            f"X and y have different number of samples: {X.shape[0]}, {y.shape[0]}"
        )
    irand = np.random.choice(X.shape[0], size=X.shape[0], replace=False)
    return X[irand], y[irand]


def _convert_dens_to_obs(apars, dens, dens_smooth, outll, mT_ll):
    """Convert density to observed coordinates.

    Adapted from:
    streamgap-pepper -- Copyright 2015-2016, Jo Bovy, Jason Sanders.
    Licensed under BSD-3-Clause.

    Parameters
    ----------
    apars: array_like
        Parallel angles.
    dens: array_like
        density(apars).
    dens_smooth: array_like
        Smooth-stream density(apars).
    outll: array_like
        Longitude.
    mT_ll: array_like
        Mean track at outll.

    Return
    ------
        (longitude, dens/dens_smooth)
    """
    mT_ll_0 = mT_ll[0]
    isort = np.argsort(mT_ll_0)  # Not all mT_ll_0 are sorted.
    ipll = interpolate.InterpolatedUnivariateSpline(mT_ll_0[isort],
                                                    apars[isort])
    ipdens = interpolate.InterpolatedUnivariateSpline(apars,
                                                      dens / dens_smooth)
    return (outll, ipdens(ipll(outll)))


def _get_track_coord_dict():
    """Get track coordinate id number given its string description,
    consistently with galpy conventions.
    """
    return {'latitude': 1, 'distance': 2, 'vlos': 3, 'pmll': 4, 'pmbb': 5}


def _convert_track_to_obs(apars, sdf_smooth, outll, mT_ll, coord):
    """Convert track to observed coordinates

    Adapted from:
    streamgap-pepper -- Copyright 2015-2016, Jo Bovy, Jason Sanders.
    Licensed under BSD-3-Clause.

    Parameters
    ----------
    apars: array_like
        Parallel angles.
    sdf_smooth: streamgapdf
        The DF of a tidal stream peppered with impacts. Required to
        convert the track to observational coordinates.
    outll: array_like
        Longitude.
    mT_ll: array_like
        Mean track at outll.
    coord: str
        Coordinate to convert to. Must be one among: latitude,
        distance, vlos, pmll, pmbb.

    Return
    ------
        (longitude, (track-smooth)[coord])
    """
    mT_ll_0 = mT_ll[0]
    isort = np.argsort(mT_ll_0)  # Not all mT_ll_0 are sorted.
    ipll = interpolate.InterpolatedUnivariateSpline(mT_ll_0[isort],
                                                    apars[isort])
    coord_dict = _get_track_coord_dict()
    if coord not in coord_dict:
        raise NotImplementedError(
            f"track coord must be one of: {[k for k, __ in coord_dict.items()]}"
        )
    coord_id = coord_dict[coord]
    ipcoord = interpolate.InterpolatedUnivariateSpline(apars, mT_ll[coord_id])
    # If performance is an issue, smooth_track and smooth_ll could be
    # passed as the same arguments for all coord values.
    smooth_track = []
    for coord in range(6):
        smooth_track.append(
            interpolate.InterpolatedUnivariateSpline(
                sdf_smooth._interpolatedThetasTrack,
                sdf_smooth._interpolatedObsTrackLB[:, coord]))
    smooth_ll = interpolate.InterpolatedUnivariateSpline(
        sdf_smooth._interpolatedObsTrackLB[:, 0],
        sdf_smooth._interpolatedThetasTrack)
    return (outll,
            ipcoord(ipll(outll)) - smooth_track[coord_id](smooth_ll(outll)))


def _dispatch_converter(dens, mT_ll, coord, apars, dens_smooth, sdf_smooth,
                        outll):
    """Dispatch over conversion method."""
    coord_dict = _get_track_coord_dict()
    if (coord != 'density') and (coord not in coord_dict):
        choices = ['density'] + [k for k, __ in coord_dict.items()]
        raise NotImplementedError(f"track coord must be one of: {choices}")
    if coord == 'density':
        return _convert_dens_to_obs(apars, dens, dens_smooth, outll, mT_ll)[1]
    else:
        return _convert_track_to_obs(apars, sdf_smooth, outll, mT_ll, coord)[1]


def _convert_parallel(coord, sdf_smooth, outll, mT_ll, ddata, nopar=False):
    """Parallelize conversion over mT_ll and ddata samples. Return array
    of (longitude, converted_quantity).

    Exceptions may be hidden by the multiprocessing module. For
    debugging purposes set `nopar=True` to compute serially.
    """
    nsamp = len(ddata[2:])
    if nsamp != mT_ll.shape[0]:
        raise RuntimeError(f"Different sample sizes: "
                           f"density ({nsamp}), mean track ({mT_ll.shape[0]})")

    if nopar:
        return np.array([
            _dispatch_converter(ddata[2 + i], mT_ll[i], coord, ddata[0],
                                ddata[1], sdf_smooth, outll)
            for i in range(nsamp)
        ])

    p = mp.Pool()
    # Convenience arrays to pass constant elements to starmap.
    coord_n = np.array([coord] * nsamp)
    apars_n = np.array([ddata[0]] * nsamp)
    dens_smooth_n = np.array([ddata[1]] * nsamp)
    sdf_smooth_n = np.array([sdf_smooth] * nsamp)
    outll_n = np.array([outll] * nsamp)

    data = np.asarray(
        p.starmap(
            _dispatch_converter,
            zip(ddata[2:], mT_ll, coord_n, apars_n, dens_smooth_n,
                sdf_smooth_n, outll_n)))
    return data


def _get_mT_parallel_helper(mO, apars, sdf_pepper):
    return sdf_pepper.meanTrack(apars, _mO=mO, coord='lb')


def _get_mT_parallel(mO, apars, sdf_pepper, nopar=False):
    if nopar:
        return np.array(
            [_get_mT_parallel_helper(apars, x, sdf_pepper) for x in mO[2:]])
    p = mp.Pool()
    apars_n = [apars] * len(mO[2:])
    sdf_pepper_n = [sdf_pepper] * len(mO[2:])
    return np.array(
        p.starmap(_get_mT_parallel_helper, zip(mO[2:], apars_n, sdf_pepper_n)))


def get_stream_data_obs(dfiles,
                        mOfiles,
                        sdf_smooth,
                        sdf_pepper,
                        coord,
                        verbose=False):
    """Return density fluctuation (relative to smooth profile) as a
    function of Galactic longitude, and respective binary
    labels. Remove samples with negative parallel frequency or density
    (due to numerical errors).

    Parameters
    -----------
    dfile: list of str
        Path to density (as function of apars) files. Each file corresponds to
        a different category.
    mOfiles: list of str
        Path to mean parallel frequency (as function of apars) files
        corresponding to different classes. Required to get density on same
        grid as track.
    sdf_smooth: streamgapdf
        The DF of a tidal stream peppered with impacts. Required to
        convert the track to observational coordinates.
    sdf_pepper: streampepperdf
        The DF of a tidal stream peppered with impacts. Required to compute
        the mean track.
    coord: str
        Conversion target. Must be one of: density, latitude,
        distance, vlos, pmll, pmbb.
    verbose: bool
        Verbose mode.

    Return
    ------
    (ll, [(X0, y0), (X1, y1), ...]: tuple
        Galactic longitude and data tensors corresponding to the different
        input file categories, including labels. If `coord=='density'`, then X
        contains `density / density_smooth`, otherwise `track - track_smooth`.

    """
    if verbose:
        print("Loading data")
    ddata = _load_data(dfiles)
    mOdata = _load_data(mOfiles)
    apars, mO_apars = ddata[0][0], mOdata[0][0]
    if np.any([not np.all(np.isclose(x, y)) for x, y in zip(apars, mO_apars)]):
        raise RuntimeError(
            "Density and frequency computed for different apars")
    if np.any([d.shape[0] != mO.shape[0] for d, mO in zip(ddata, mOdata)]):
        raise RuntimeError("Different number of density and frequency samples")
    if verbose:
        print("Computing main tracks")
    # List of tensors of shape (nsamples, ncoords, napars)
    mT_ll = [_get_mT_parallel(mO, apars, sdf_pepper) for mO in mOdata]
    ll = np.linspace(max([np.amin([x[0] for x in mT]) for mT in mT_ll]),
                     min([np.amax([x[0] for x in mT]) for mT in mT_ll]),
                     len(apars))
    if verbose:
        print("Converting coordinates")
    Xs = [
        _convert_parallel(coord, sdf_smooth, ll, mT, d)
        for mT, d in zip(mT_ll, ddata)
    ]
    return ll, _get_shuffled_X_y(*Xs)


def get_sdf_smooth_pepper(pepperfilename='gd1pepper1sampling.pkl'):
    """Load the smooth and peppered stream required for conversion
    to observed space.

    """
    sdf_smooth = gd1_util.setup_gd1model()
    if os.path.exists(pepperfilename):
        with open(pepperfilename, 'rb') as savefile:
            sdf_pepper = pickle.load(savefile)
    else:
        age = 9  # Default age value in simulate_streampepper script.
        # Only need 1 time-sampling, because only use it to convert to
        # observed space at present.
        timpacts = simulate_streampepper.parse_times('1sampling', age)
        sdf_pepper = gd1_util.setup_gd1model(timpact=timpacts,
                                             hernquist=True,
                                             age=age)
        save_pickles(pepperfilename, sdf_pepper)
    return sdf_smooth, sdf_pepper


def compute_data_Qrho(dfiles,
                      pfile,
                      smoothpepper_file='gd1pepper1sampling.pkl'):
    """Convert `streampepper` simulations output stored in dfiles to density
    perturbations as functions of longitude.

    Parameters
    ----------
    dfiles: list of str
        Paths to simulate_streampepper output files.
    pfile_root: str
        Path to output pickle file that will contain a dictionary with
        longitude values, corresponding density perturbations X (tensor of
        shape (nsamples, nlongitudes) for each input file) and labels y (tensor
        of shape (nsamples,) containing labels 0, 1, ... for samples from
        dfiles[0], dfiles[1], ..., respectively).

    """
    sdf_smooth, sdf_pepper = get_sdf_smooth_pepper(smoothpepper_file)
    ll, (X, y) = get_stream_data_obs(
        dfiles, [df.replace('dens', 'omega') for df in dfiles],
        sdf_smooth,
        sdf_pepper,
        'density',
        verbose=True)
    data = {"longitude": ll, "X": X, "y": y}
    with open(pfile, 'wb') as fp:
        pickle.dump(data, fp)


def load_data_Qrho(pfile, sigma_shot=0.0):
    """Load simulations data pickled by `compute_data_Qrho`. Return density X as a
    tensor of shape (samples, longitude_bins), and corresponding labels y of
    shape (samples). `sigma_shot` is shot noise error (fractional error
    relative to smooth density profile).

    """
    with open(pfile, 'rb') as filename:
        data = pickle.load(filename)
    ll, X_dens, y_dens_f = data["longitude"], data["X"], data["y"]
    y_dens = y_dens_f.astype(int)
    if not np.all(y_dens == y_dens_f):
        raise RuntimeError(
            'Casting to integer labels left non-zero remainder(s)')
    # Negative densities at this point are due to numerical integration issues.
    mask = [np.all(x >= 0) for x in X_dens]
    print(f"Filtered {X_dens.shape[0] - X_dens[mask].shape[0]} dens<0 samples")
    # Now we add noise and allow for negative sampled values (the *expectation*
    # value must be positive, not necessarily the *sampled* ones).
    X_dens = add_noise(X_dens, sigma_shot)
    return ll, X_dens[mask], y_dens[mask]

#!/usr/bin/env python3

# Copyright 2020 Francesco Montanari
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Model selection based on PDF analysis."""

import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from numpy.lib import scimath

import stream_utils as stu


def print_dbg(*x):
    print('dbg: ', end='')
    print(*x)


def _savefig(fig, fname):
    fig.savefig(fname)
    print(f'Saved {fname}')


def set_matplotlib_font(size):
    font = {'size': size}
    matplotlib.rc('font', **font)


def plot_dens(masses, datadir, savedir, sigma=None):
    """Plot density profiles. If `sigma` is not None, add Gaussian error with
    standard deviation sigma * rho_smooth.

    """
    data = np.loadtxt(os.path.join(
        datadir, f"20200316/gd1_t64sampling_X5_{masses}_dens.dat"),
                      delimiter=",")
    apars = data[0]
    rho_smooth = data[1]
    mask = [np.all(x >= 0) for x in data]
    rho = data[mask][2:]
    if sigma is not None:
        rho = np.array(
            [np.random.normal(x, sigma * np.abs(rho_smooth)) for x in rho])
    fig, ax = plt.subplots()
    [ax.plot(apars, x, 'grey', alpha=0.1) for x in rho]
    idx = np.random.randint(0, len(rho), size=10)
    [ax.plot(apars, rho[i], 'darkred') for i in idx]
    ax.plot(apars, rho_smooth, 'k--')
    m1, m2 = masses.split('-')
    ax.set_title(f"$10^{m1}-10^{m2} M_\\odot$")
    ax.set_xlabel("$\\Delta\\theta_{\\parallel}$")
    ax.set_ylabel(r"$\rho / \bar\rho_0$")
    fig.savefig(os.path.join(savedir, f"dens_{masses}.png"))


def _get_Qrho_lims(X_dens, y_dens):
    """Compute largest common range where all y_dens labels have samples."""
    masks = [y_dens == i for i in np.unique(y_dens)]
    xmins = [np.amin(X_dens[m]) for m in masks]
    xmaxs = [np.amax(X_dens[m]) for m in masks]
    return max(xmins), min(xmaxs)


def _get_centers(bins):
    """Compute centers given histogram bin edges."""
    return [0.5 * (bins[i] + bins[i + 1]) for i in range(len(bins) - 1)]


def _compute_pdfpdf_hists(pdf_pdfs, num):
    """Compute histograms given a list of PDF(PDF(Qrho)) samples."""
    xmin = min([min(p) for p in pdf_pdfs])
    xmax = max([max(p) for p in pdf_pdfs])
    bins = np.unique(np.linspace(xmin, xmax, num))
    hists = [np.histogram(p, bins=bins, density=True)[0] for p in pdf_pdfs]
    return hists, bins


def _compute_evidences(like, prior0, prior1, bins):
    """Integrate like * prior0 and like * prior1 using trapezoidal rule."""
    E0 = np.sum(like * prior0 * np.diff(bins))
    E1 = np.sum(like * prior1 * np.diff(bins))
    if (E0 > 0) and (E1 > 0):
        return E0, E1
    else:
        return np.nan, np.nan


def _get_interq_evidence(lnB):
    """Compute interquantile range formatted for plotting."""
    y = []
    yerr = []
    for x in lnB:
        q1, q2, q3 = np.quantile(x, [0.25, 0.5, 0.75])
        yerr += [[q2 - q1, q3 - q2]]
        y += [q2]
    y = np.array(y)
    yerr = np.array(yerr).T
    return y, yerr


class PdfPlotter:
    def __init__(self, X_dens, y_dens, labels=None, nbin=20, savedir='plots'):
        """Parameters
        ----------

        X_dens: (nsamples, nlong) array
            Samples of density perturbations as function of longitude.

        y_labels: (nsamples,) array
            Labels ()ordered list of consecutive integers starting from 0)
            corresponding to each sample.

        labels: list of str
            Labels descriptions, one for each y_dens class.

        nbin: int
            Number of bin in density to compute the PDF.

        savedir: str
            Output directory.

        """
        self.labels = labels
        self.classes = np.unique(y_dens)
        self._check_classes()
        self.nbin = nbin
        min_Qrho, max_Qrho = _get_Qrho_lims(X_dens, y_dens)
        self.pdf_bins = np.linspace(min_Qrho, max_Qrho, num=self.nbin + 1)
        self.pdf_bin_centers = _get_centers(self.pdf_bins)
        self.pdf_Qrhos = [
            self._get_hist(X_dens[y_dens == i]) for i in self.classes
        ]
        self.savedir = self._get_savedir(savedir)

    def _check_classes(self):
        if not np.all(self.classes == np.arange(len(self.classes))):
            raise RuntimeError(
                'Classes must be ordered integers starting form 0')
        if ((self.labels is not None)
                and (len(self.labels) != len(self.classes))):
            raise RuntimeError(
                f'Mismatch: {len(self.labels)} labels, but {len(self.classes)} classes'
            )

    def _get_hist(self, X):
        return np.array(
            [np.histogram(x, bins=self.pdf_bins, density=True)[0] for x in X])

    def _get_savedir(self, savedir):
        savedir = os.path.join(
            savedir, f'pdf_pdf_{self.nbin}bin_{len(self.classes)}classes/')
        if not os.path.exists(savedir):
            os.makedirs(savedir)
        return savedir

    def _h_plot_pdf_interq(self, ax, pdf_Qrho, label=''):
        # Plot the interquantile range and the median for the PDF.
        x = self.pdf_bin_centers
        y = []
        yerr = []
        for i in range(len(pdf_Qrho.T)):
            q1, q2, q3 = np.quantile(pdf_Qrho.T[i], [0.25, 0.5, 0.75])
            yerr += [[q2 - q1, q3 - q2]]
            y += [q2]
        y = np.array(y)
        yerr = np.array(yerr).T
        Mmin, Mmax = label.split('-')
        _, caps, _ = ax.errorbar(x,
                                 y,
                                 yerr=yerr,
                                 fmt='o-',
                                 capsize=4,
                                 label=label)
        ax2 = ax.twiny()
        ax2.set_xlim(ax.get_xlim())
        ax2.set_xticks(x)
        step = 5 if (len(x) <= 50) else 10
        ax2.set_xticklabels(
            [i if (i % step == 0) else '' for i, __ in enumerate(x)])
        ax2.set_xlabel('$\\alpha$')

    def plot_pdf_interq(self):
        """Plot the PDF interquantile range at each PDF bin."""
        fig, ax = plt.subplots()
        for i in self.classes:
            label = None if self.labels is None else self.labels[i]
            self._h_plot_pdf_interq(ax, self.pdf_Qrhos[i], label=label)
        ax.set_xlabel(r'$Q\rho$')
        ax.set_ylabel('PDF')
        ax.legend()
        _savefig(fig, os.path.join(self.savedir, 'dens_pdf_interq.png'))

    def plot_pdf_pdf(self):
        """Plot PDF(PDF(Qrho)) for each PDF(Qrho) bin."""
        for i in range(self.nbin):
            fig, ax = plt.subplots()
            hists, bins = _compute_pdfpdf_hists(
                [p.T[i] for p in self.pdf_Qrhos], num=50)
            centers = _get_centers(bins)
            for j in self.classes:
                if ((i > 0) or self.labels is None):
                    label = ''
                else:
                    label = self.labels[j]
                ax.hist(centers,
                        bins=bins,
                        weights=hists[j],
                        histtype='step',
                        label=label)
            plt.gca().set_prop_cycle(None)  # Reset line colors.
            ax.set_xlabel(f'$y_{{{i}}}$', fontsize=24)
            if i == 0:
                ax.legend(prop={'size': 24})
            fig.tight_layout()
            _savefig(fig, os.path.join(self.savedir, f'pdf_pdf_{i}.png'))

    def _compute_lnB(self, fid_i):
        """Return a dict lnB_i[j] for j != i. Each dict value is a tensor of
        shape (pdf_nbins, pdf_pdf_nbins). The first dimension refers to the
        alpha PDF bin. The second dimension refers to a
        $lnB_{alpha, ij}^{fid, i}$ value computed with different choice for the
        number of PDF(PDF(Qrho)_alpha) bins.

        """
        jset = set(self.classes) - {fid_i}
        lnBi = {}
        for j in jset:
            lnBi[j] = []
        for alpha in range(self.nbin):
            for j in jset:
                lnBij_alpha = []
                for num in np.arange(10, 500, 10):
                    hists, bins = _compute_pdfpdf_hists(
                        [p.T[alpha] for p in self.pdf_Qrhos], num=num)
                    like = hists[fid_i]
                    prior_i, prior_j = hists[fid_i], hists[j]
                    Ei, Ej = _compute_evidences(like, prior_i, prior_j, bins)
                    lnBij_alpha += [scimath.log(Ei / Ej)]
                lnBi[j] += [lnBij_alpha]
        for j in jset:
            lnBi[j] = np.array(lnBi[j])
        return lnBi

    def _plot_shades(self, ax, lims):
        if lims is not None:
            Qrho_min, Qrho_max = lims
            imin = np.where(self.pdf_bins > Qrho_min)[0]
            imax = np.where(self.pdf_bins > Qrho_max)[0]
            imin = imin[0] if (imin.size > 0) else 0
            imax = imax[0] if (imax.size > 0) else self.nbin
        else:
            imin, imax = 0, self.nbin
        if imin > 0:
            ax.axvspan(0, imin - 0.5, alpha=0.5, color='grey')
        if imax < self.nbin:
            ax.axvspan(imax + 0.5, self.nbin, alpha=0.5, color='grey')
        return imin, imax

    def plot_lnB(self, lims=None):
        """Plot the log of the Bayes factors. Error bars are interquantile
        ranges obtained varying the binnins of PDF(PDF(Qrho)). Compute
        cumulative evidence only within `lims`.

        """
        # lnB: dictionary with `(i, j)` keys referring to $lnB_{ij}^{fid, i}$.
        # Each value is a tensor of shape (pdf_nbins, pdf_pdf_nbins).
        lnB = {}
        for i in self.classes:
            for j, val in self._compute_lnB(i).items():
                lnB[(i, j)] = val
        fig, ax = plt.subplots()
        alpha_min, alpha_max = self._plot_shades(ax, lims)
        logs = ''
        for (i, j), lnBij in lnB.items():
            y, yerr = _get_interq_evidence(lnBij)
            label = rf'$\log B_{{{i}{j}}}^{{{{\rm fid}}, {i}}}$'
            ax.errorbar(list(range(self.nbin)),
                        y,
                        yerr=yerr,
                        fmt='o',
                        capsize=4,
                        label=label)
            logs += f'Cumulative {label}: {sum(y[alpha_min:alpha_max]):0.1f}\n'
        ax.set_xlabel(r'$\alpha$')
        ax.xaxis.set_major_locator(plt.MaxNLocator(integer=True))
        ax.legend()
        fig.tight_layout()
        _savefig(fig, os.path.join(self.savedir, 'lnB.png'))
        with open(os.path.join(self.savedir, 'lnB.log'), 'w') as f:
            f.write(logs)


def run_2classes_plots(datadir, savedir, compute_obs_density=False):
    """Run PDF analysis assuming 2 mass classes."""
    if compute_obs_density:
        dfile0 = os.path.join(datadir,
                              '20200316/gd1_t64sampling_x5_3-5_dens.dat')
        dfile1 = os.path.join(datadir,
                              '20200316/gd1_t64sampling_x5_5-9_dens.dat')
        # Compute and pickle smooth stream and coordinate conversion. Can take
        # a few minutes.
        stu.compute_data_Qrho(
            [dfile0, dfile1],
            os.path.join(datadir, 'scripts_data/xy_obs_density_2classes.pkl'),
            os.path.join(datadir, 'scripts_data/gd1pepper1sampling.pkl'))
    __, X_dens, y_dens = stu.load_data_Qrho(os.path.join(
        DATADIR, 'scripts_data/Xy_obs_density_2classes.pkl'),
                                            sigma_shot=0.1)
    make_label = lambda Mmin, Mmax: fr'$10^{Mmin}-10^{Mmax}\ M_\odot$'
    for i in [10, 20, 50, 100]:
        pdfplt = PdfPlotter(X_dens,
                            y_dens,
                            labels=[make_label(3, 5),
                                    make_label(5, 9)],
                            nbin=i,
                            savedir=savedir)
        pdfplt.plot_pdf_interq()
        pdfplt.plot_pdf_pdf()
        pdfplt.plot_lnB(lims=(0.75, 1.25))


def run_3classes_plots(datadir, savedir, compute_obs_density=False):
    """Run PDF analysis assuming 2 mass classes."""
    if compute_obs_density:
        dfile0 = os.path.join(datadir,
                              '20200316/gd1_t64sampling_x5_3-5_dens.dat')
        dfile1 = os.path.join(datadir,
                              '20200316/gd1_t64sampling_x5_5-7_dens.dat')
        dfile2 = os.path.join(datadir,
                              '20200316/gd1_t64sampling_x5_7-9_dens.dat')
        # Compute and pickle smooth stream and coordinate conversion. Can take
        # a few minutes.
        stu.compute_data_Qrho(
            [dfile0, dfile1, dfile2],
            os.path.join(datadir, 'scripts_data/xy_obs_density_3classes.pkl'),
            os.path.join(datadir, 'scripts_data/gd1pepper1sampling.pkl'))
    __, X_dens, y_dens = stu.load_data_Qrho(os.path.join(
        datadir, 'scripts_data/Xy_obs_density_3classes.pkl'),
                                            sigma_shot=0.1)
    make_label = lambda Mmin, Mmax: fr'$10^{Mmin}-10^{Mmax}\ M_\odot$'
    for i in [10, 20, 50, 100]:
        pdfplt = PdfPlotter(
            X_dens,
            y_dens,
            labels=[make_label(3, 5),
                    make_label(5, 7),
                    make_label(7, 9)],
            nbin=i,
            savedir=savedir)
        pdfplt.plot_pdf_interq()
        pdfplt.plot_pdf_pdf()
        pdfplt.plot_lnB(lims=(0.7, 1.3))


if __name__ == "__main__":
    set_matplotlib_font(14)
    DATADIR = '/home/francesco/Data/streams/'
    SAVEDIR = '/home/francesco/Dropbox/Work/streams/plots/'
    run_2classes_plots(DATADIR, SAVEDIR)
    # run_3classes_plots(DATADIR, SAVEDIR)
    plot_dens("3-5", DATADIR, SAVEDIR)
    plot_dens("5-7", DATADIR, SAVEDIR)
    plot_dens("7-9", DATADIR, SAVEDIR)
    plot_dens("5-9", DATADIR, SAVEDIR)
